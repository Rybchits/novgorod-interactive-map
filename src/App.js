import React from "react";
import {Navbar} from "./components/Navbar";
import {RoutesList} from "./components/RoutesList"
import {InteractiveMap} from "./components/InteractiveMap";
import {RouteDescription} from "./components/RouteDescription";

function App() {

  return (
    <div>
      <Navbar/>
        <InteractiveMap/>
        <RoutesList />
        <RouteDescription/>
    </div>
  );
}

export default App;