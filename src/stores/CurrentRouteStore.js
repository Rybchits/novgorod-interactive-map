import {createStore, createEvent} from "effector/compat";

export const setCurrentRoute = createEvent();

export const $currentRoute = createStore({
    id: 1, name: 'У ИСТОКОВ РОССИЙСКОЙ ГОСУДАРСТВЕННОСТИ',  description: "Хотите удалить информацию о веб-страницах, которые посещали в Chrome? Очистите историю браузера частично или полностью. Эти изменения коснутся всех устройств, на которых включена синхронизация в Chrome.\n" +
        "\n" +
        "История просмотров будет удалена из браузера Chrome. Вы можете также удалить историю поиска из своего аккаунта Google.",  isInteractive: true,
    image: "https://novgorod.travel/upload/iblock/07b/07b2936d264af02c0c0a62eb27810009.jpg",
    type: "История", points: [[58.521743, 31.263357], [58.524652, 31.261321], [58.526610, 31.252222]]})
    .on(setCurrentRoute, (state, value) => state = value);