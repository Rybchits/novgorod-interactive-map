import {createStore, createEvent} from "effector/compat";

export const setCurrentAppStore= createEvent();

export const $currentAppState = createStore(false).on(setCurrentAppStore, (state, value) => state = value);