import {createStore} from "effector/compat";

export const $routes = createStore([{
    id: 1, name: 'У истоков государственности',  description: "",  isInteractive: true, movingMethod: "BUS", duration: '2 дня/1 ночь',
    image: "https://novgorod.travel/upload/iblock/07b/07b2936d264af02c0c0a62eb27810009.jpg",
    type: "Наука, История", points: [[58.521743, 31.263357], [58.524652, 31.261321], [58.526610, 31.252222]]},

    {id: 2,  name: 'Купеческая дочь', isInteractive: true, description: "   В программе: Вас ждёт очень познавательное и в то же время развлекательное путешествие во времени с купеческой дочерью Настасьей," +
            " которая проведёт гостей по Ярославову Дворищу и поведает увлекательные истории о купцах новгородских, да заморских, об иностранных дворах на торгу, о самой знаменитой средневековой новгородке.", movingMethod: "WALK", duration: '40 минут',
        image: "https://novgorod.travel/upload/iblock/1f5/1f5a33754a6eb7ee785b665409b29843.jpg", type: "Культура",
        points: [[58.522881, 31.271296], [58.521424, 31.273446], [58.518260, 31.283652]]},

    {id: 3, name: 'Источник Марка Пустынина', isInteractive: false, description: "", movingMethod: "CAR", duration: '2 часа',
        image: "https://novgorod.travel/upload/iblock/80d/80d8b6f8fa0a2175ae6a818c5a8dd6f5.jpg", type: "Природа. Необычные места.",
        points: [[58.521743, 31.263357], [58.524652, 31.261321], [58.526610, 31.252222]]},

    {id: 4, name: 'Уездной город', isInteractive: false, description: "", movingMethod: "WALK", duration: "2 часа",
        image: "https://novgorod.travel/upload/iblock/f10/f10c4aa28d1cf01c0f983f003a036fd5.jpg", type:"История",
        points: [[58.522881, 31.271296], [58.521424, 31.273446], [58.518260, 31.283652]]
    }])