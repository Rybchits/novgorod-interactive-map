import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import logo from '../logo.svg';
import {Avatar} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
        },
        appBar: {
            backgroundColor: 'white',
            boxShadow: 'none',
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
            color: "darkblue",
            marginLeft: theme.spacing(1),
        },
        hide: {
            display: 'none',
        },
        logo: {
            height: '3rem',
            width: '3rem',
        }
    }),
);

export const Navbar = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar className={classes.appBar} position='static'>
                <Toolbar>
                    <img src={logo} className={classes.logo} alt="logo"/>
                    <Typography variant='h6' className={classes.title}>
                        TYRMAP
                    </Typography>
                    <Avatar>RG</Avatar>
                </Toolbar>
            </AppBar>
        </div>

    );
}
