import React from "react";
import {
    List,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    Typography
} from "@material-ui/core";
import Rating from '@material-ui/lab/Rating';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    container: {
        background: 'white',
        height: '100rem'
    }
}))

export const RatingPlaces = () => {
    const places = [{name: "Площадь Победы-Софийская", rating: 4, type: "Площадь"},
        {name: "Памятник Тысячелетия России", rating: 5, type: "Памятник"},
        {name: "Николо-Дворищенский собор", rating: 4.5, type: "Собор"}
    ]

    return (
        <div>
            <Typography variant={'body2'}>Рейтинг точек маршрута</Typography>
            <List>
                {places.map( (value) =>
                    <ListItem>
                        <ListItemText
                            primary={value.name}
                            secondary={value.type}
                        />
                        <ListItemSecondaryAction>
                            <Rating name="half-rating" defaultValue={value.rating} precision={0.5} />
                        </ListItemSecondaryAction>
                    </ListItem>
                )}
            </List>
        </div>
    );
}