import React, {useEffect} from "react";
import {
    Button,
    IconButton,
    ImageList,
    ImageListItem,
    ImageListItemBar,
    SwipeableDrawer,
    Typography
} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {useStore} from "effector-react";
import {$currentAppState, setCurrentAppStore} from "../stores/CurrentAppStateStore";
import {$currentRoute} from "../stores/CurrentRouteStore";
import ExploreIcon from '@material-ui/icons/Explore';
import DirectionsBusIcon from "@material-ui/icons/DirectionsBus";
import DirectionsWalkIcon from "@material-ui/icons/DirectionsWalk";
import DirectionsCarIcon from "@material-ui/icons/DirectionsCar";
import ScheduleIcon from '@material-ui/icons/Schedule';

const useStyles = makeStyles((theme) => ({
    container: {
        background: 'white',
        height: '100rem'
    },
    title: {
        fontSize: 14,
        color: "darkblue",
        fontFamily: 'Roboto'
    },
    rootImage: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    imageList: {
        width: "100rem",
        height: "18rem",
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
    typeTextKW: {
        color: "darkblue",
        textIndent: '1.5em',
        textAlign: 'left',
        height: theme.spacing(4),
        position: 'absolute',
        top: '16rem',
        fontSize: 14,
        left: '0'
    },
    description: {
        paddingLeft: theme.spacing(3),
        marginTop: theme.spacing(1)
    },
    detailsText: {
        color: "#858585",
        fontSize: 18
    },
    iconDetails: {
        height: theme.spacing(3),
        width: theme.spacing(3),
        color: "#858585",
        marginLeft: theme.spacing(1)
    },
    row: {
        display: "flex",
        margin: theme.spacing(1, 0)
    },
    enterBtn: {
        margin: theme.spacing(2,2),
        bottom: 0
    }
}));

function InfoIcon() {
    return null;
}

export const RouteDescription = () => {
    const classes = useStyles();
    const appState = useStore($currentAppState)
    const route = useStore($currentRoute)

    useEffect(() => {
        //setCurrentAppStore(true);
        console.log("useEffect RouteDescription")
    }, [appState])

    const toggleDrawer = (anchor, open) => (event) => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setCurrentAppStore(open);
    }


    return (<>
            <React.Fragment key={'bottom'}>
                <SwipeableDrawer
                    anchor={'bottom'}
                    open={appState}
                    onClose={toggleDrawer('bottom', false)}
                    onOpen={toggleDrawer('bottom', true)}
                    style={{minHeight: "500px", minWidth: "500px"}}
                >
                    <div className={classes.container}>
                        <div className={classes.rootImage}>

                            <ImageList rowHeight={250} gap={1} cols={1} className={classes.imageList}>
                                <ImageListItem key={route.image}>
                                    <img src={route.image} alt={route.name}/>
                                    <ImageListItemBar
                                        title={route.name}
                                        actionIcon={
                                            <IconButton aria-label={`info about ${route.name}`}
                                                        className={classes.icon}>
                                                <InfoIcon/>
                                            </IconButton>
                                        }
                                    />
                                </ImageListItem>

                            </ImageList>

                            <Typography variant="caption" className={classes.typeTextKW}>
                                {"Ключевые слова: " + route.type}
                            </Typography>

                            <div>
                                {route.isInteractive &&
                                    <div className={classes.row}>
                                        <Typography className={classes.detailsText} variant={'h6'}>Интерактивный</Typography>
                                        <ExploreIcon className={classes.iconDetails}/>
                                    </div>
                                }
                            </div>
                            <div className={classes.row}>
                                {route.movingMethod === "BUS" &&
                                <>
                                    <Typography className={classes.detailsText} variant={'h6'}>На автобусе</Typography>
                                    <DirectionsBusIcon className={classes.iconDetails}/>}
                                </>}
                                {route.movingMethod === "WALK" &&
                                <>
                                    <Typography className={classes.detailsText} variant={'h6'}>Пешком</Typography>
                                    <DirectionsWalkIcon className={classes.iconDetails}/>
                                </>}
                                {route.movingMethod === "CAR" &&
                                <>
                                    <Typography className={classes.detailsText} variant={'h6'}>На машине</Typography>
                                    <DirectionsCarIcon className={classes.iconDetails}/>
                                </>}
                            </div>

                            <div className={classes.row}>
                                <Typography className={classes.detailsText} variant={'h6'}>Длительность: {route.duration}</Typography>
                                <ScheduleIcon className={classes.iconDetails}/>
                            </div>

                            <Typography variant="body1" className={classes.description}>
                                {route.description}
                            </Typography>

                            <Button className={classes.enterBtn}
                                    variant="contained"
                                    fullWidth={true} color={"primary"}>Хочу пройти маршрут!</Button>
                        </div>
                    </div>
                </SwipeableDrawer>
            </React.Fragment>
        </>
    )
}