import React, {useRef} from "react";
import {Drawer} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import ReactCardCarousel from "react-card-carousel";
import {RouteCard} from "./RouteCard";
import {useStore} from "effector-react";
import {$routes} from "../stores/RoutesStore";
import {$currentRoute, setCurrentRoute} from "../stores/CurrentRouteStore";

const useStyles = makeStyles((theme) => ({
    drawer: {
        height: '45%',
        flexShrink: 0,
    },
    drawerPaper: {
        height: '45%',
        backgroundColor: '#f2f2f200',
        border: 'none'
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 2),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'space-between',
    },
    routesList: {
        position: "relative",
        height: "100%",
        width: "100%",
        display: "flex",
        flex: 1,
        justifyContent: "center",
        alignItems: "middle"
    },
    details: {

    }
}));

export const RoutesList = () => {
    const carousel = useRef();
    const routes = useStore($routes);
    const classes = useStyles();
    const route = useStore($currentRoute)

    const handleChangeCard = () => {
        setCurrentRoute(routes[carousel.current.getCurrentIndex()])
        console.log(route)
    }

    return (
        <Drawer
            className={classes.drawer}
            variant="persistent"
            anchor="bottom"
            open={true}
            classes={{
                paper: classes.drawerPaper,
            }}
        >
            <ReactCardCarousel className={classes.routesList} ref={carousel} afterChange={handleChangeCard}>
                {routes.map((route) => <RouteCard route={route}/>)}
            </ReactCardCarousel>

        </Drawer>
    )
}