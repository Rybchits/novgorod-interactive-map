import React, {useEffect, useRef, useState} from "react";
import {YMaps, Map, GeolocationControl, Button} from "react-yandex-maps";
import {useStore} from "effector-react";
import {$currentRoute} from "../stores/CurrentRouteStore";

export const InteractiveMap = () => {
    const currentRoute = useStore($currentRoute);
    const [defaultState, setDefaultState] = useState({ center: [58.52, 31.26], zoom: 14 });
    const map = useRef(null);
    const [ymaps, setYMaps] = useState(null);

    const addRoute = (ymaps) => {
        map.current.geoObjects.removeAll();
        const multiRoute = new ymaps.multiRouter.MultiRoute(
            {
                referencePoints: [...currentRoute.points],
                params: {
                    routingMode: "pedestrian"
                }
            },
            { boundsAutoApply: true }
        );

        map.current.geoObjects.add(multiRoute);
    };

    useEffect(() => {
        if (ymaps != null && map != null){
            addRoute(ymaps)
        }
    }, [currentRoute])

    return (
        <div className="App">
            <YMaps query={{ apikey: process.env.REACT_APP_YANDEX_MAPS_API_KEY }}>
                <div>
                    <Map height="30rem"
                         width="100%"
                         defaultState={defaultState}
                         instanceRef={map}
                         modules={["multiRouter.MultiRoute"]}
                         onLoad={ymaps => {
                             addRoute(ymaps)
                             setYMaps(ymaps)
                         }}
                    >
                        <Button
                            options={{ maxWidth: 150, position: { bottom: 160, right: 10} }}
                            data={{ content: 'Фильтрация' }}
                            onClick={() => console.log('click')}
                        />
                        <GeolocationControl options={{ maxWidth: 150, position: { bottom: 160, left: 10} }} />
                    </Map>
                </div>
            </YMaps>
        </div>
    );
}
