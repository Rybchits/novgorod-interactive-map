import React from "react";
import {Card, CardActionArea, CardMedia, CardContent, Typography } from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {useStore} from "effector-react";
import {setCurrentAppStore} from "../stores/CurrentAppStateStore";
import {$currentRoute} from "../stores/CurrentRouteStore";
import ExploreIcon from "@material-ui/icons/Explore";
import DirectionsBusIcon from '@material-ui/icons/DirectionsBus';
import DirectionsWalkIcon from '@material-ui/icons/DirectionsWalk';
import DirectionsCarIcon from '@material-ui/icons/DirectionsCar';

const useStyles = makeStyles((theme) => ({
    root: {
        height: theme.spacing(40),
        width: theme.spacing(40),
        color: "black",
        fontFamily: "sans-serif",
        borderRadius: "10px",
        boxSizing: "border-box"
    },
    title: {
        fontSize: 14,
        color: "darkblue",
        fontFamily: 'Roboto'
    },
    image: {
        width: "100%"
    },
    imgWrapper: {
        maxHeight: "10rem",
        overflow: "hidden",
        paddingTop:"0",
        position: 'absolute',
        top:'0'
    },
    typeText: {
        color: "#858585"
    },
    iconRow: {
        display: "flex",
        marginTop: theme.spacing(1),
    }
}));

export const RouteCard = ({route}) => {
    const classes = useStyles();

    const mainRoute = useStore($currentRoute);

    const onDescription = (event) => {
        console.log(event)
        setCurrentAppStore(true);
    }

    return(
        <Card className={classes.root}>
            <CardActionArea style={{height:"100%",  textAlign:"top"}} onClick={(mainRoute.id === route.id)?onDescription:null}>
                <CardMedia
                    title="Contemplative Reptile"
                >
                    <div className={classes.imgWrapper}><img className={classes.image} src={route.image}/></div>
                </CardMedia>
                <CardContent style = {{position: 'absolute',
                    top:'10rem'}}>
                    <Typography gutterBottom variant="h5" component="h2"  >
                        {route.name}
                    </Typography>
                    <Typography variant="body2" className={classes.typeText} component="p">
                        {route.type}
                    </Typography>
                    <div className={classes.iconRow}>
                        <>
                            {route.isInteractive && <ExploreIcon className={classes.typeText}/>}
                            {route.movingMethod === "BUS" && <DirectionsBusIcon className={classes.typeText}/>}
                            {route.movingMethod === "WALK" && <DirectionsWalkIcon className={classes.typeText}/>}
                            {route.movingMethod === "CAR" && <DirectionsCarIcon className={classes.typeText}/>}
                        </>
                        <Typography style={{marginLeft: "0.5rem", paddingTop: "0.1rem"}}
                                    variant={'body2'}>
                            {route.duration}
                        </Typography>
                    </div>
                </CardContent>
            </CardActionArea>
        </Card>
    )
}